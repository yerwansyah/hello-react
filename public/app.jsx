{/*app.jsx*/ }

var React = require('react');
var ReactDOM = require('react-dom');

var Greeter = require('Greeter');

ReactDOM.render(
    <Greeter name="Yusrizal" message="This is message from property!" />,
    document.getElementById('app')
);