# Hello React

## Synopsis
This project is a Brief introduction about Node.js, and how to create hello world project using node.js, express and React.

<br/>
To use this project just run the following command:
```
npm install -g webpack
npm install
webpack
node server.js
```

## Getting Started
Before we create React Project, we need the following object/application. Please following the official website for installing the tools.
* Node.js
* Code Editor e.g Visual Studio Code

### Setup the project
#### Environment Checking
* Open terminal or command prompt
* Go to any directory that you will be used as working directory
* Create new directory hello-react using command `mkdir hello-react`
* run the following command to check node version and npm version that already installed

    `$ node -v`
    
    `$ npm -v`
    
If Node and npm are installed correctly, than it will display the version.

#### Step 1 Init Project
To init the project, run the following command 

`$ npm init`

It will display the explanation for initiate the project. Please follow the snippet

```
This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg> --save` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
name: (HelloReact) hello-react
version: (1.0.0)
description: Simple React App
entry point: (index.js)
test command:
git repository:
keywords:
author: Yusrizal Erwansyah
license: (ISC) MIT
About to write to D:\GIT\gitlab\hello-react\package.json:

{
  "name": "hello-react",
  "version": "1.0.0",
  "description": "Simple React App",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Yusrizal Erwansyah",
  "license": "MIT"
}


Is this ok? (yes)
```
After this you will have package.json in your working directory

```
D:\GIT\gitlab\hello-react>dir
 Volume in drive D is DATA
 Volume Serial Number is 58EF-7BA0

 Directory of D:\GIT\gitlab\hello-react

30-Dec-16  14:29    <DIR>          .
30-Dec-16  14:29    <DIR>          ..
30-Dec-16  14:24               291 package.json
30-Dec-16  14:36               329 README.md
               2 File(s)            620 bytes
               2 Dir(s)  63,682,424,832 bytes free
```

package.json
```json
{
  "name": "hello-react",
  "version": "1.0.0",
  "description": "Simple React App",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Yusrizal Erwansyah",
  "license": "MIT"
}
```

Next Step is installing express using npm, run the following command `npm install express --save`

It will add dependency package into package.json then install express library in node_modules directory. Following is the new package.json
```json
{
  "name": "hello-react",
  "version": "1.0.0",
  "description": "Simple React App",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Yusrizal Erwansyah",
  "license": "MIT",
  "dependencies": {
    "express": "^4.14.0"
  }
}
```

Next add new file to root directory of project, name it **server.js**. Add the following code.
```javascript
//server.js

var express = require('express');

//Create our app
var app = express();

app.use(express.static('public'));

app.listen(3000, function(){
    console.log('Express server is up on port 3000');
});
```

Next create public directory in root directory, name it **public**. Then add new file in public directory, name it **index.html** and add the following code.
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
    </head>

    <body>
        <h1>Hello World!!!</h1>
    </body>
</html>
```

Next we can run our web server using the following command `node server.js`
```
D:\GIT\gitlab\hello-react>node server.js
Express server is up on port 3000
```
Next open the browser and go to **http:\\localhost:3000**. It will display our Hello World

This is the end of Step 1 Init Project. All this step is in step1 branch. You can do `git checkout step1` to get everything in this Introduction.


#### Step 2 Hello React !!!
Open our index.html file and edit the code as follow:
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.7/react.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.7/react-dom.js"></script>
    </head>

    <body>
        <div id="app"></div>

        <script type="text/babel">
            ReactDOM.render(
                <h1>Hello React!!!</h1>,
                document.getElementById('app')
            );
        </script>
    </body>
</html>
```

Now run again the project and open again in the browser to check the changes. In here we add the React render directly in the **index.html** file.

Next we will split the rendering into other file. Now Create a file under public directory, name it **app.jsx**. Add the React Dom render to the file as follow
```jsx
{/*app.jsx*/}

ReactDOM.render(
    <h1>Hello React!!!</h1>,
    document.getElementById('app')
);
```
Then remove the code from **index.html** and load the app.jsx in the script as follow
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.7/react.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.7/react-dom.js"></script>
    </head>

    <body>
        <div id="app"></div>

        <script type="text/babel" src="app.jsx"></script>
    </body>
</html>
```

Next we will edit again the app.jsx to add our react components. We will move the **<h1>** tag from React Render to React Class component then call the component into React Render as follows:
```jsx
{/*app.jsx*/}

var Greeter = React.createClass({
    render: function(){
        return(
            <div>
                <h1> Hello React </h1>
                <p>This is from a component</p>
            </div>
        );
    }
});

ReactDOM.render(
    <Greeter/>,
    document.getElementById('app')
);
```

In that code snippet, we user `React.createClass`. In createClass we have render object that will return our html view. We can add any html tag, but we can only have one root tag, in this sample we use `<div>`. Inside `<div>` tag we can define any tags to create our HTML Page.

This is the end of Step 2.

#### Step 3 More React Components, Properties, Reference and State

In this step 3 we will use React Components, Properties, Reference and State, then Use events and callbacks.

Now we will add `name`property into our class, use the property value and display it into our web app. Edit app.jsx as follow:
```jsx
{/*app.jsx*/ }

var Greeter = React.createClass({
    getDefaultProps: function () {
        return {
            name: 'React'
        }
    },
    render: function () {
        var name = this.props.name

        return (
            <div>
                <h1> Hello {name}! </h1>
                <p>This is from a component</p>
            </div>
        );
    }
});

ReactDOM.render(
    <Greeter />,
    document.getElementById('app')
);
```

In that code, we add new variable `name` and set the value to react property name using `this.props.name`. As we can see, the `name` property does not have any value. So we define it's default property using `getDefaultProps` and set the `name` default value. If you not define default value, it will only display **Hello !**.

Now we can passing the property value from our `ReactDOM.render` just like any other html tag property as follow:
```jsx
ReactDOM.render(
    <Greeter name="Yusrizal" />,
    document.getElementById('app')
);
```

If we remove the name property then it will display the default value. So this is how you can create react property, use it and display it.
Next we can create any other property to our React Class Component. Now edit our app.jsx as follows:
```jsx
{/*app.jsx*/ }

var Greeter = React.createClass({
    getDefaultProps: function () {
        return {
            name: 'React',
            message: 'This is default message!'
        }
    },
    render: function () {
        var name = this.props.name;
        var message = this.props.message;

        return (
            <div>
                <h1> Hello {name}! </h1>
                <p>{message + '!!'}</p>
            </div>
        );
    }
});

ReactDOM.render(
    <Greeter name="Yusrizal" message="This is message from property!"/>,
    document.getElementById('app')
);
```

You can do any experiment to our app.jsx to see the changes.

Next we will create the event handler to get the input value from the user and use it. We will define `<form>` tag and add any input tag. In each input tag, we must define `ref` property. This `ref` property will be used as refence which components that we use to get the value. After that we will define the event handler. You can edit the app.jsx as follows:
```jsx
{/*app.jsx*/ }

var Greeter = React.createClass({
    getDefaultProps: function () {
        return {
            name: 'React',
            message: 'This is default message!'
        }
    },
    onButtonClick: function(e){
        e.preventDefault();

        var name = this.refs.name.value;
        alert (name);
    },
    render: function () {
        var name = this.props.name;
        var message = this.props.message;

        return (
            <div>
                <h1> Hello {name}! </h1>
                <p>{message + '!!'}</p>

                <form onSubmit={this.onButtonClick}>
                    <input type="text" ref="name" />
                    <button>Set Name</button>
                </form>
            </div>
        );
    }
});

ReactDOM.render(
    <Greeter name="Yusrizal" message="This is message from property!" />,
    document.getElementById('app')
);
```

As you can see, we not use `this.props` object, but we will use `this.refs` object. After we get the user input value, then we can use the value. This sample we will update the name that already displayed to any value that is inputted by the user.
In React, we cannot edit the property value, so this time we will use state. You can edit the app.jsx as follow:
```jsx
{/*app.jsx*/ }

var Greeter = React.createClass({
    getDefaultProps: function () {
        return {
            name: 'React',
            message: 'This is default message!'
        }
    },
    getInitialState: function () {
        return {
            name: this.props.name
        }
    },
    onButtonClick: function (e) {
        e.preventDefault();

        var nameRef = this.refs.name;
        var name = nameRef.value;

        nameRef.value = '';

        if (name.length > 0) {
            this.setState({
                name: name
            });
        }
    },
    render: function () {
        var name = this.state.name;
        var message = this.props.message;

        return (
            <div>
                <h1> Hello {name}! </h1>
                <p>{message + '!!'}</p>

                <form onSubmit={this.onButtonClick}>
                    <input type="text" ref="name" />
                    <button>Set Name</button>
                </form>
            </div>
        );
    }
});

ReactDOM.render(
    <Greeter name="Yusrizal" message="This is message from property!" />,
    document.getElementById('app')
);
```

You can see, we change `var name = this.props.name;` to `var name = this.state.name`. We also define initial value for the state so that when it's load for the first time, it will still display the value. To edit state value, we cannot do as usual javascript, we must use `setState`. You can also do the experiment so you can understand more.

Next task is to create other input to get the message, and change the display as the user input. There are many ways to achieve that and you can create the logic as you want. The following is one of the sample.
```jsx
{/*app.jsx*/ }

var Greeter = React.createClass({
    getDefaultProps: function () {
        return {
            name: 'React',
            message: 'This is default message!'
        }
    },
    getInitialState: function () {
        return {
            name: this.props.name,
            message: this.props.message
        }
    },
    onButtonClick: function (e) {
        e.preventDefault();

        var name = this.refs.name.value;
        var message = this.refs.message.value;

        var updates = {};

        if (name.length > 0) {
            updates.name = name;
            this.refs.name.value = '';
        }

        if (message.length > 0) {
            updates.message = message;
            this.refs.message.value = '';
        }

        this.setState(updates);
    },
    render: function () {
        var name = this.state.name;
        var message = this.state.message;

        return (
            <div>
                <h1> Hello {name}! </h1>
                <p>{message + '!!'}</p>

                <form onSubmit={this.onButtonClick}>
                    <div>
                        <input type="text" ref="name" placeholder="Input Name" />
                    </div>
                    <div>
                        <textarea ref="message" placeholder="Input Message" />
                    </div>
                    <div>
                        <button>Set Data</button>
                    </div>
                </form>
            </div>
        );
    }
});

ReactDOM.render(
    <Greeter name="Yusrizal" message="This is message from property!" />,
    document.getElementById('app')
);
```

After all of this, i hope you understand about React Component, Property, Reference, State and how to add Event Handler.

This is the end of Step 3.

#### Step 4 React Nested Component

In this step we learn about React Nested Component. In Previous Step we already create `Greeter` component. Now we will refactoring the component. We will create two new components `GreeterForm` and `GreeterMessage`. `GreeterForm` contains everything about the form submit. `GreeterMessage` contains about displaying the data as follow:
```jsx
{/*app.jsx*/ }

var GreeterMessage = React.createClass({
    render: function () {
        var name = this.props.name;
        var message = this.props.message;

        return (
            <div>
                <h1>Hello {name}! </h1>
                <p>{message}</p>
            </div>
        );
    }
});

var GreeterForm = React.createClass({
    onFormSubmit: function (e) {
        e.preventDefault();

        var name = this.refs.name.value;
        var message = this.refs.message.value;

        var updates = {};

        if (name.length > 0) {
            updates.name = name;
            this.refs.name.value = '';
        }

        if (message.length > 0) {
            updates.message = message;
            this.refs.message.value = '';
        }

        this.props.onNewData(updates);
    },
    render: function () {
        return (
            <form onSubmit={this.onFormSubmit}>
                <div>
                    <input type="text" ref="name" placeholder="Input Name" />
                </div>
                <div>
                    <textarea ref="message" placeholder="Input Message" />
                </div>
                <div>
                    <button>Set Data</button>
                </div>
            </form>
        );
    }
});

var Greeter = React.createClass({
    getDefaultProps: function () {
        return {
            name: 'React',
            message: 'This is default message!'
        }
    },
    getInitialState: function () {
        return {
            name: this.props.name,
            message: this.props.message
        }
    },
    handleNewData: function (updates) {
        this.setState(updates);
    },
    render: function () {
        var name = this.state.name;
        var message = this.state.message;

        return (
            <div>
                <GreeterMessage name={name} message={message}/>
                <GreeterForm onNewData={this.handleNewData} />
            </div>
        );
    }
});

ReactDOM.render(
    <Greeter name="Yusrizal" message="This is message from property!" />,
    document.getElementById('app')
);
```

As you can see, in `Greeter` component only contains the tag for `GreeterMessage` and `GreeterForm`. This is how to call another react components. It applies just like an html tag.

Next, we will separating the components into different files.

First, we need some third party library dependencies so we can split the components into different files. We need `webpack` to packing the components into one bundle file. We also need react library so we can import the module into our jsx files.
Use the following command to install webpack library globally so we can use it in our terminal or commandprompt.

`sudo npm install -g webpack`

After that, install all the third library dependencies locally and update the `package.config`:

`sudo npm install --save react react-dom`

Next we also need to add dev dependencies, this library will be only used for development:

`sudo npm install --save-dev webpack babel-core babel-loader babel-preset-es2015 babel-preset-react`

After installation success, we are ready to splitting our components.

Create new directory `components` under `public` directory. Next create three new files `Greeter.jsx`, `GreeterMessage.jsx`, `GreeterForm.jsx` under `components` directory.
After that, move the components from app.jsx to the file that having the same name. Import react library and export the module for all jsx file:
```jsx
{/*GreeterMessage.jsx*/ }

var React = require('react');

var GreeterMessage = React.createClass({
    render: function () {
        var name = this.props.name;
        var message = this.props.message;

        return (
            <div>
                <h1>Hello {name}! </h1>
                <p>{message}</p>
            </div>
        );
    }
});

module.exports = GreeterMessage;
```

```jsx
{/*GreeterForm.jsx*/ }

var React = require('react');

var GreeterForm = React.createClass({
    onFormSubmit: function (e) {
        e.preventDefault();

        var name = this.refs.name.value;
        var message = this.refs.message.value;

        var updates = {};

        if (name.length > 0) {
            updates.name = name;
            this.refs.name.value = '';
        }

        if (message.length > 0) {
            updates.message = message;
            this.refs.message.value = '';
        }

        this.props.onNewData(updates);
    },
    render: function () {
        return (
            <form onSubmit={this.onFormSubmit}>
                <div>
                    <input type="text" ref="name" placeholder="Input Name" />
                </div>
                <div>
                    <textarea ref="message" placeholder="Input Message" />
                </div>
                <div>
                    <button>Set Data</button>
                </div>
            </form>
        );
    }
});

module.exports = GreeterForm;
```

```jsx
{/*Greeter.jsx*/ }

var React = require('react');
var GreeterForm = require('./GreeterForm.jsx');
var GreeterMessage = require('./GreeterMessage.jsx');

var Greeter = React.createClass({
    getDefaultProps: function () {
        return {
            name: 'React',
            message: 'This is default message!'
        }
    },
    getInitialState: function () {
        return {
            name: this.props.name,
            message: this.props.message
        }
    },
    handleNewData: function (updates) {
        this.setState(updates);
    },
    render: function () {
        var name = this.state.name;
        var message = this.state.message;

        return (
            <div>
                <GreeterMessage name={name} message={message}/>
                <GreeterForm onNewData={this.handleNewData} />
            </div>
        );
    }
});

module.exports = Greeter;
```

Update `app.jsx`
```jsx
{/*app.jsx*/ }

var React = require('react');
var ReactDOM = require('react-dom');

var Greeter = require('./components/Greeter.jsx');

ReactDOM.render(
    <Greeter name="Yusrizal" message="This is message from property!" />,
    document.getElementById('app')
);
```

Update `index.html`
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
    </head>

    <body>
        <div id="app"></div>

        <script src="bundle.js"></script>
    </body>
</html>
```

Create new `webpack.config.js`
```javascript
module.exports = {
    entry: './public/app.jsx',
    output: {
        path: __dirname,
        filename: './public/bundle.js'
    },
    resolve: {
        root: __dirname,
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                },
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/
            }
        ]
    }
}
```

After all the files ready, next we pack all the components into `bundle.js`. Run the following command:

`webpack` to build only one time.

`webpack -w` to build and waiting if any other changes to build again.

Our Web App is ready, run the application then running on the browser.


##### Explanation
As you can see, in `app.jsx`, all of the code moved to `Greeter.jsx`, `GreeterMessage.jsx`, `GreeterForm.jsx`.

In `index.html`, we removed all the react component references, because it's already bundle in `bundle.js`. We also changed the script to load `bundle.js` instead of `app.jsx`.

For `webpack.config.js` as the configuration file for `webpack`. We define everything in this file config.
* `entry` is used to define the entry point of the webpack for building the bundle.
* `output` is used to define the result file after webpack successfully build the bundle.
* `resolve` is used to define which extension files will be proceed by webpack.
* `module` is used to define what module that needed by the webpack to build the bundle.

That all default value that will be used for the config file, so that you can use that config file for another project with only small changes.

As you can see, in `app.jsx` and `Greeter.jsx`, we imported the other components. We used the path of the file location. If you want to use the components in other directory, you need to specify the directory again. With webpack, we can define the alias in `webpack.config.js` for each file as follow:
```javascript
module.exports = {
    entry: './public/app.jsx',
    output: {
        path: __dirname,
        filename: './public/bundle.js'
    },
    resolve: {
        root: __dirname,
        alias: {
            Greeter: 'public/components/Greeter.jsx',
            GreeterMessage: 'public/components/GreeterMessage.jsx',
            GreeterForm: 'public/components/GreeterForm.jsx'
        },
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                },
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/
            }
        ]
    }
}
```

Then we can remove all the path directory when imported the modules and only use the alias name as follow:
```jsx
{/*app.jsx*/ }

var React = require('react');
var ReactDOM = require('react-dom');

var Greeter = require('Greeter');

```

```jsx
{/*Greeter.jsx*/ }

var React = require('react');
var GreeterForm = require('GreeterForm');
var GreeterMessage = require('GreeterMessage');
```

This is the end of the Step 4 and also the end of Hello React Project.

The next project is createing Boilerplate project as project base when starting new react project. Boilerplate project is created base on this project. Just removed everything components that refer to Greeter object. You can also get the Boilerplate project from the following command:

`git clone https://gitlab.com/yerwansyah/react-boilerplate.git`

I hope you understand the basic of the React while doing this project. Thanks.

## License
MIT License